<html>
	<head>
		<title>Laravel</title>
		
		<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('bootstrap/css/bootstrap-theme.min.css') }}" rel="stylesheet">
	</head>
	<body>
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <blockquote id="status">
                        Status Here
                    </blockquote>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    {!! Form::model($order, ['action' => ['WelcomeController@add', 'id'=>$order->id], 'id' => 'someform']) !!}
                        <div class="form-group">
                            <label for="name">Product name</label>
                            {!! Form::hidden('id', null, ['id' => 'id']) !!}
                            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Product name']) !!}
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity in stock</label>
                            {!! Form::text('quantity', null, ['id' => 'quantity', 'class' => 'form-control', 'placeholder' => 'Quantity in stock']) !!}
                        </div>
                        <div class="form-group">
                            <label for="price">Price per item</label>
                            {!! Form::text('price', null, ['id' => 'price', 'class' => 'form-control', 'placeholder' => 'Price per item']) !!}
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    {!! Form::close() !!}
    			</div>
			</div>
		</div>
        <script
                src="https://code.jquery.com/jquery-3.1.1.min.js"
                integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
                crossorigin="anonymous"></script>
		<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
		<script>
            $.fn.serializeObject = function()
            {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
            $(document).ready(function(){
//                $.ajaxSetup({
//                    headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
//                });
                $('#someform').on('submit', function(){
                    $.ajax({
                        url:'{!! action('WelcomeController@add', ['id'=>$order->id]) !!}',
                        dataType: 'json',
                        method: 'POST',
                        data: $( this ).serializeObject()
                    })
                            .done(function(data){
                                $('#status')
                                    .removeClass()
                                    .addClass('bg-success')
                                    .html($("<a>").attr('href', data.url).text(data.url));
                            })
                            .fail(function(data){
                                $('#status')
                                    .removeClass()
                                    .addClass('bg-danger')
                                    .text("Some errors");
                            });
                    return false;
                });
            });
        </script>
	</body>
</html>
