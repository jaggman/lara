<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Order extends Model {

    protected $fillable = [
        'id', 'name', 'quantity', 'price', 'total', 'dt',
    ];

    private static $_json;
    private static $_pop;

    public function __construct(array $attributes)
    {
        self::upJson();
        parent::__construct($attributes);
    }

    private static function upJson()
    {
        if (!Storage::disk('local')->exists('some.json')) {
            self::$_json = [];
            self::$_pop = 0;
            Storage::disk('local')->put('some.json', json_encode([0]));
        }
        if (self::$_json === null) {
            $json = Storage::disk('local')->get('some.json');
            self::$_json = json_decode($json, true);
            self::$_pop = array_pop(self::$_json);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
        ]);
    }


    public function save(array $options = array())
    {
        $this->price = (float) $this->price;
        $this->quantity = (float) $this->quantity;
        $this->total = $this->price * $this->quantity;
        $this->dt = date('Y-m-d H:i:s');
        $order = static::findOne($this->id);
        self::$_pop -= $order->total;
        if ($order->id) unset(self::$_json[$order->id]);

        if ($this->id) {
            self::$_json[$this->id] = $this->attributesToArray();
        } else {
            $key = max(array_keys(self::$_json));
            $this->id = $key === false ? 0 : $key;
            $this->id++;
            self::$_json[$this->id] = $this->attributesToArray();
        }
        self::$_pop += $this->total;
        Storage::disk('local')->put('some.json', json_encode(self::$_json+[self::$_pop]));
        return true;
    }

    public static function findOne($id)
    {
        self::upJson();
        return new static(array_key_exists($id, self::$_json) ? self::$_json[$id] : []);
    }
}
